package com.example.steph.fyp;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.steph.fyp.helpers.DateHelper;
import com.example.steph.fyp.models.Appointment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by steph on 11/11/2017.
 */

public class AppointmentList extends ArrayAdapter<Appointment> {

    private Activity context;
    private List<Appointment> appointments;

    public AppointmentList(Activity context, List<Appointment> appointmentList){
        super(context,R.layout.appointmentlist_layout, appointmentList);
        this.context = context;
        this.appointments = appointmentList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.appointmentlist_layout, null, true);

        TextView textViewDate = (TextView) listViewItem.findViewById(R.id.textview_date);
        TextView textViewStartTime = (TextView) listViewItem.findViewById(R.id.textview_starttime);

        Appointment appointment = appointments.get(position);


        DateFormat df = new SimpleDateFormat("EEE, d MMM");
        Date date = new Date(appointment.getStartTime());

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int hours = calendar.get(Calendar.HOUR_OF_DAY);
        DateHelper dateHelper = new DateHelper();


        textViewStartTime.setText(dateHelper.toAppointmentTime(appointment.getStartTime()));
        textViewDate.setText(df.format(appointment.getStartTime()));

        return listViewItem;
    }
}
