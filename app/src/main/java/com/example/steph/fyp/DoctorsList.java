package com.example.steph.fyp;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.steph.fyp.helpers.DateHelper;
import com.example.steph.fyp.models.Appointment;
import com.example.steph.fyp.models.Doctors;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by steph on 11/11/2017.
 */

public class DoctorsList extends ArrayAdapter<Doctors> {

    private Activity context;
    private List<Doctors> doctors;

    public DoctorsList(Activity context, List<Doctors> doctorsList){
        super(context,R.layout.change_doctor_dialog, doctorsList);
        this.context = context;
        this.doctors = doctorsList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.doctorlist_layout, null, true);

        TextView textViewName = (TextView) listViewItem.findViewById(R.id.doctors_listview_name);
        ImageView imageViewPhoto = (ImageView) listViewItem.findViewById(R.id.doctors_listview_photo);

        Doctors doctor = doctors.get(position);


        textViewName.setText(doctor.getName());
        Context context = imageViewPhoto.getContext();
        int id = context.getResources().getIdentifier(doctor.getPhoto(), "drawable", context.getPackageName());
        imageViewPhoto.setImageResource(id);

        return listViewItem;
    }

}
