package com.example.steph.fyp;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.steph.fyp.helpers.DateHelper;
import com.example.steph.fyp.models.Appointment;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

/**
 * Created by steph on 06/02/2018.
 */

public class AppointmentAdapter extends RecyclerView.Adapter<AppointmentAdapter.AppointmentViewHolder> {

    private Context mCtx;
    private List<Appointment> appointmentList;

    public AppointmentAdapter(Context mCtx, List<Appointment> appointmentList) {
        this.mCtx = mCtx;
        this.appointmentList = appointmentList;
    }

    @Override



    public AppointmentViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(mCtx);
        View view = inflater.inflate(R.layout.appointments_recyclerview_layout,null );
        return new AppointmentViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AppointmentViewHolder holder, int position) {
        Appointment appointment = appointmentList.get(position);

        DateFormat df = new SimpleDateFormat("EEE, d MMM");
        Date date = new Date(appointment.getStartTime());

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int hours = calendar.get(Calendar.HOUR_OF_DAY);
        DateHelper dateHelper = new DateHelper();

        holder.textViewRating.setText("test");
        holder.textViewName.setText(appointment.getDoctorName());
        holder.textViewDate.setText(df.format(appointment.getStartTime()));
        holder.textViewTime.setText(dateHelper.toAppointmentTime(appointment.getStartTime()));

        /*Context context = holder.imageView.getContext();
        int id = context.getResources().getIdentifier(appointment.getDoctorPhoto(), "drawable", context.getPackageName());
        holder.imageView.setImageResource(id);*/
    }

    @Override
    public int getItemCount() {
        return appointmentList.size();
    }

    class AppointmentViewHolder extends RecyclerView.ViewHolder {
        ImageView imageView;
        TextView textViewRating, textViewDate, textViewName, textViewTime;
        public AppointmentViewHolder(View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.appoinmentListImageView1);
            textViewDate = itemView.findViewById(R.id.appointmentListDate);
            textViewName = itemView.findViewById(R.id.appointmentListDocName);
            textViewRating = itemView.findViewById(R.id.textViewRating);
            textViewTime = itemView.findViewById(R.id.appointmentListTime);

        }
    }

}