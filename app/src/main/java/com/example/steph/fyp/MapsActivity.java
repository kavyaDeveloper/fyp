package com.example.steph.fyp;

import android.content.Context;
import android.content.Intent;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.steph.fyp.helpers.AccountsHelper;
import com.example.steph.fyp.models.ClinicInfo;
import com.example.steph.fyp.models.Users;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback {

    private GoogleMap mMap;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

        final TextView clinicAddress = (TextView) findViewById(R.id.clinic_address_textview);
        final TextView clinicPhoneNum = (TextView) findViewById(R.id.clinic_phone_textview);
        final TextView clinicName = (TextView) findViewById(R.id.clinic_name_textview);
        final ImageView clinicImage = (ImageView) findViewById(R.id.clinic_image);



        final AccountsHelper accountsHelper = new AccountsHelper();
        accountsHelper.getCurrentUser(new BookAppointment.DataListener() { //TODO NOTE here is method to get current users details

            @Override
            public void newDataReceived(Users user) {
                DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("clinics").child(user.getClinic()).child("information");
                databaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        ClinicInfo clinic = dataSnapshot.getValue(ClinicInfo.class);
                        LatLng location = new LatLng(clinic.getLatitude(), clinic.getLongitude());
                        mMap.addMarker(new MarkerOptions().position(location).title(clinic.getName()));
                        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(location, 15));

                        clinicName.setText(clinic.getName());
                        clinicPhoneNum.setText(clinic.getPhone());
                        clinicAddress.setText(clinic.getAddress());

                        Context context = clinicImage.getContext();
                        int id = context.getResources().getIdentifier(clinic.getPhoto(), "drawable", context.getPackageName());
                        clinicImage.setImageResource(id);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        // ...
                    }
                });
            }
        });
    }

    public interface DataListener {
        void newDataReceived(Users user);
    }



    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        // Add a marker in Sydney and move the camera
        LatLng sydney = new LatLng(4, 3);
        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }
}
