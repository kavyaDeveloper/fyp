package com.example.steph.fyp.models;

/**
 * Created by steph on 04/01/2018.
 */

public class ClinicInfo {
    private String address;
    private float longitude;
    private float latitude;
    private String name;
    private String phone;
    private String photo;

    public ClinicInfo(){

    }

    public ClinicInfo(String address, float longitude, float latitude, String name, String phone, String photo) {
        this.address = address;
        this.longitude = longitude;
        this.latitude = latitude;
        this.name = name;
        this.phone = phone;
        this.photo = photo;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public float getLongitude() {
        return longitude;
    }

    public void setLongitude(float longitude) {
        this.longitude = longitude;
    }

    public float getLatitude() {
        return latitude;
    }

    public void setLatitude(float latitude) {
        this.latitude = latitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }
}
