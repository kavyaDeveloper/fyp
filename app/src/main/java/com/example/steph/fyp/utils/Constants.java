package com.example.steph.fyp.utils;

/**
 * Created by steph on 17/11/2017.
 */

public class Constants {
    public static final String USER_KEY = "users";
    public static final String BOOKING_KEY = "bookings";
    public static final String APPOINTMENT_KEY = "appointments";
    public static final String CLINICS_KEY = "clinics";

}
