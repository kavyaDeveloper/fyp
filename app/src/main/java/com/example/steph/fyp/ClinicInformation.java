package com.example.steph.fyp;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.example.steph.fyp.helpers.AccountsHelper;
import com.example.steph.fyp.models.ClinicInfo;
import com.example.steph.fyp.models.Users;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class ClinicInformation extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_clinic_information);


        final TextView clinicAddress = (TextView) findViewById(R.id.clinic_address_textview1);
        final TextView clinicPhoneNum = (TextView) findViewById(R.id.clinic_phone_textview1);
        final TextView clinicName = (TextView) findViewById(R.id.clinic_name_textview1);
        final String[] geoLocation = new String[2];

        clinicAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intentBundle = new Intent(ClinicInformation.this, MapsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putString("lat", geoLocation[0]);
                bundle.putString("long", geoLocation[1]);
                //bundle.putString("clinicName", clinicName);
                intentBundle.putExtras(bundle);


                startActivity(intentBundle);
            }

        });


        final AccountsHelper accountsHelper = new AccountsHelper();
        accountsHelper.getCurrentUser(new BookAppointment.DataListener() { //TODO NOTE here is method to get current users details

            @Override
            public void newDataReceived(Users user) {
                DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("clinics").child(user.getClinic()).child("information");
                databaseRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        ClinicInfo clinic = dataSnapshot.getValue(ClinicInfo.class);

                        geoLocation[0] = Float.toString(clinic.getLatitude());
                        geoLocation[1] = Float.toString(clinic.getLongitude());

                        clinicName.setText(clinic.getName());
                        clinicPhoneNum.setText(clinic.getPhone());
                        clinicAddress.setText(clinic.getAddress());
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {
                        // ...
                    }
                });
            }
        });
    }
    public interface DataListener {
        void newDataReceived(Users user);
    }

}
