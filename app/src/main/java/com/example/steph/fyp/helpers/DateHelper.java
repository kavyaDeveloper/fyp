package com.example.steph.fyp.helpers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by steph on 14/11/2017.
 */

public class DateHelper {
    public DateHelper() {

    }

    public String toAppointmentTime(long dateNum){
        Date date = new Date(dateNum);
        DateFormat dateFormat = new SimpleDateFormat("hh:mm a");

        return dateFormat.format(date);
    }

    public String toAppointmentDate(long dateNum){
        Date date = new Date(dateNum);
        SimpleDateFormat formatter = new SimpleDateFormat("EEE, MMM d");
        return  formatter.format(date).toString();
    }

    public boolean isDateSameDay(long firstDate, long secondDate){
        Calendar cal1 = Calendar.getInstance();
        Calendar cal2 = Calendar.getInstance();
        Date date1 = new Date(firstDate);
        Date date2 = new Date(secondDate);

        cal1.setTime(date1);
        cal2.setTime(date2);
        boolean sameDay = cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
        return  sameDay;
    }

    public long getStartOfDayInMilli(long milliseconds){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliseconds);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DATE);
        calendar.set(year, month, day, 0, 0, 0);
        return calendar.getTimeInMillis();
    }

    public long getEndOfDayInMilli(long milliseconds) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(milliseconds);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DATE);
        calendar.set(year, month, day, 23, 59, 59);
        return calendar.getTimeInMillis();
    }

    public String displayMilliInHrsMins(long milliseconds) {
        String format = "%02d:%02d";

        return String.format(format,
                TimeUnit.MILLISECONDS.toHours(milliseconds),
                TimeUnit.MILLISECONDS.toMinutes(milliseconds) - TimeUnit.HOURS.toMinutes(
                        TimeUnit.MILLISECONDS.toHours(milliseconds))
                );
    }
}
