package com.example.steph.fyp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

import com.example.steph.fyp.helpers.DateHelper;
import com.example.steph.fyp.models.Bookings;
import com.example.steph.fyp.utils.Constants;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.Calendar;

public class QueueStatusActivity extends AppCompatActivity {
    TextView queueStatusTextView;
    String clinicId;
    long delay;
    long appointmentLength = 900000; //15 minutes
    DateHelper dateHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_queue_status);


        DateHelper dateHelper = new DateHelper();
        queueStatusTextView = (TextView) findViewById(R.id.queueStatusTextview);
        Intent myIntent = getIntent(); // gets the previously created intent
        clinicId = myIntent.getStringExtra("clinic");
        dateHelper = new DateHelper();
        long time = Calendar.getInstance().getTimeInMillis();

        final DateHelper finalDateHelper = dateHelper;
        FirebaseDatabase.getInstance().getReference(Constants.BOOKING_KEY).orderByChild("startTime").startAt(dateHelper.getStartOfDayInMilli(time)).endAt(dateHelper.getEndOfDayInMilli(time)).addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        //iterating through all the nodes
                        if(dataSnapshot.getChildrenCount() == 0){
                            queueStatusTextView.setText("You dont have any appointments today");
                        }
                        else {
                            for (DataSnapshot appointmentSnapshot : dataSnapshot.getChildren()) {
                                //getting artist

                                Bookings booking = appointmentSnapshot.getValue(Bookings.class);
                                if(booking.getClinicId().equals(clinicId) && booking.getCheckOutTime() > 0){
                                    delay = booking.getCheckOutTime() - (booking.getStartTime() + appointmentLength);
                                }

                            }
                            queueStatusTextView.setText(finalDateHelper.displayMilliInHrsMins(delay));
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
    }
}
