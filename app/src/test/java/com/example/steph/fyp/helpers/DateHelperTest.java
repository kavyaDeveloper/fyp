package com.example.steph.fyp.helpers;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Calendar;
import java.util.Date;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.lessThan;
import static org.junit.Assert.assertThat;

import static org.hamcrest.CoreMatchers.is;


import static org.junit.Assert.*;

/**
 * Created by steph on 02/03/2018.
 */
public class DateHelperTest {
    @Before
    public void setUp() throws Exception {
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void toAppointmentTime() throws Exception {
    }

    @Test
    public void toAppointmentDate() throws Exception {
    }

    @Test
    public void isDateSameDay() throws Exception {

            long firstDate = 1520075295049L; //Sat Mar 03 2018 11:08:15
            long secondDate = 1520120000000L; //Sat Mar 03 2018 23:33:20

            Calendar cal1 = Calendar.getInstance();
            Calendar cal2 = Calendar.getInstance();
            Date date1 = new Date(firstDate);
            Date date2 = new Date(secondDate);

            cal1.setTime(date1);
            cal2.setTime(date2);
            boolean sameDay = cal1.get(Calendar.YEAR) == cal2.get(Calendar.YEAR) &&
                    cal1.get(Calendar.DAY_OF_YEAR) == cal2.get(Calendar.DAY_OF_YEAR);
            assertThat(sameDay, is(true));

    }

    @Test
    public void getStartOfDayInMilli() throws Exception {
        //long input = 1520000737860L; //Fri Mar 02 2018 14:25:37
        long startOfDay = 1519948801000L; //Fri Mar 02 2018 00:00:01
        long endOfDay = 1520035199999L; // Fri Mar 02 2018 23:59:59


        long expectedLowestOutput = 1519948800000L; //Fri Mar 02 2018 00:00:00
        long expectedHigestOutput = 1519948800999L; //Fri Mar 02 2018 00:00:00

        long input = ThreadLocalRandom.current().nextLong(startOfDay, endOfDay);

        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(input);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DATE);
        calendar.set(year, month, day, 0, 0, 0);
        assertThat(calendar.getTimeInMillis(), allOf(greaterThan(expectedLowestOutput), lessThan(expectedHigestOutput)));

    }

    @Test
    public void getEndOfDayInMilli() throws Exception {
        long startOfDay = 1519948800000L; //Fri Mar 02 2018 00:00:00
        long endOfDay = 1520035199999L; // Fri Mar 02 2018 23:59:59

        long expectedLowestOutput = 1520035199000L;
        long expectedHigestOutput = 1520035199999L;

        long input = ThreadLocalRandom.current().nextLong(startOfDay, endOfDay);


        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(input);
        int year = calendar.get(Calendar.YEAR);
        int month = calendar.get(Calendar.MONTH);
        int day = calendar.get(Calendar.DATE);
        calendar.set(year, month, day, 23, 59, 59);
        assertThat(calendar.getTimeInMillis(), allOf(greaterThan(expectedLowestOutput), lessThan(expectedHigestOutput)));
    }

    @Test
    public void displayMilliInHrsMins() throws Exception {
    }

}