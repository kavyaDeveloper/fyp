package com.example.steph.fyp;

import android.app.DatePickerDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.nfc.NdefMessage;
import android.nfc.NfcAdapter;
import android.os.Bundle;
import android.os.Parcelable;
import android.provider.CalendarContract;
import android.support.design.widget.NavigationView;
import android.support.design.widget.TabLayout;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.example.steph.fyp.helpers.AccountsHelper;
import com.example.steph.fyp.helpers.DateHelper;
import com.example.steph.fyp.models.Appointment;
import com.example.steph.fyp.models.Bookings;
import com.example.steph.fyp.models.Doctors;
import com.example.steph.fyp.models.Users;
import com.example.steph.fyp.utils.Constants;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.io.UnsupportedEncodingException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class NavDrawerActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener  {

    private ImageView mDisplayImageView;
    private TextView mNameTextView;
    private TextView mEmailTextView;
    DatabaseReference databaseAppointmentRef;


    long selectedDate;
    TextView numAppointmentsTextView;
    TextView doctorSelectedName;
    List<Appointment> appointmentList;
    DateHelper dateHelper = new DateHelper();
    Users currentUser;
    List<Doctors> doctorsList = new ArrayList<>();
    String currentDoctorId;
    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    DateFormat df  = new SimpleDateFormat("EEEE, dd MMMM");
    TextView currenDateTextView;
    Button openDatePickerButton;
    DatePickerDialog datePickerDialog;
    private TabLayout tabLayout;
    ImageView rightArrowImageView;
    ImageView leftArrowImageView;
    private ProgressBar spinner;

    /*nfc declearation*/
    public final String ERROR_DETECTED = "No NFC tag detected!";
    NfcAdapter nfcAdapter;
    PendingIntent pendingIntent;
    IntentFilter writeTagFilters[];
    /*--------end---------*/

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nav_drawer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //to display loading prett

        spinner = (ProgressBar) findViewById(R.id.progressBarContentNavDrawer);

        spinner.setVisibility(View.VISIBLE);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tabLayout = (TabLayout) findViewById(R.id.tabs);

        datePickerDialog = new DatePickerDialog(this);
        //tabLayoutFragment = new TabLayoutFragment(); TODO uncommented
        GenerateAppointments gen = new GenerateAppointments();
        //gen.makeAppointments();
        getIds();

        setEvents();

        //getting the reference of appointments node
        databaseAppointmentRef = FirebaseDatabase.getInstance().getReference(Constants.APPOINTMENT_KEY);
        appointmentList = new ArrayList<>();
        View navHeaderView = navigationView.getHeaderView(0);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));
        currenDateTextView.setText(df.format(Calendar.getInstance().getTimeInMillis()));

        FirebaseDatabase.getInstance().getReference(Constants.USER_KEY).child(mFirebaseUser.getEmail().replace(".", ","))
                .addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        if (dataSnapshot.getValue() != null) {
                            currentUser = dataSnapshot.getValue(Users.class);
                            currentDoctorId = currentUser.getDoctor(); //TODO needs to be changed to name or different ui idea
                            //changeDoctorButton.setText(currentDoctorId);
                            doctorsList = getDoctorsList(currentUser.getClinic());
                            Glide.with(NavDrawerActivity.this)
                                    .load(currentUser.getPhotUrl())
                                    .into(mDisplayImageView);

                            mNameTextView.setText(currentUser.getUser());
                            mEmailTextView.setText(currentUser.getEmail());

                            //populateTabLayout(doctorsList); // TODO call to db inside call to db this ok???

                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });


        String email = FirebaseAuth.getInstance().getCurrentUser().getEmail(); //TODO clean up code here, move to login success

        DatabaseReference notificationReference = FirebaseDatabase.getInstance().getReference("notifications");
        notificationReference.child(email.replace(".", ",")).child("token").setValue(FirebaseInstanceId.getInstance()
                .getToken());

        mDisplayImageView = (ImageView) navHeaderView.findViewById(R.id.imageView_display);
        mNameTextView = (TextView) navHeaderView.findViewById(R.id.textview_nav_name);
        mEmailTextView = (TextView) navHeaderView.findViewById(R.id.textview_nav_email);
        spinner.setVisibility(View.GONE);

       /*--------Detect NFC Reader-------------*/

        nfcAdapter = NfcAdapter.getDefaultAdapter(this); //Detect NFC in device
        if (nfcAdapter == null) {
          Toast.makeText(this,"This device doesn't support NFC.", Toast.LENGTH_LONG).show();
        }else{
            Toast.makeText(this,"This device support NFC.", Toast.LENGTH_LONG).show();
        }
      /*--------------END------------*/

        readNFCIntent(getIntent());

        pendingIntent = PendingIntent.getActivity(this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
        IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
        tagDetected.addCategory(Intent.CATEGORY_DEFAULT);
        writeTagFilters = new IntentFilter[] { tagDetected };
    }


    /* Read NFC Tags*/
    private void readNFCIntent(Intent intent) {
        String action = intent.getAction();
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_TECH_DISCOVERED.equals(action)
                || NfcAdapter.ACTION_NDEF_DISCOVERED.equals(action)) {
            Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
            NdefMessage[] msgs = null;
            if (rawMsgs != null) {
                msgs = new NdefMessage[rawMsgs.length];
                for (int i = 0; i < rawMsgs.length; i++) {
                    msgs[i] = (NdefMessage) rawMsgs[i];
                }
            }
            buildNFCTagViews(msgs);
        }
    }



    /*Decode NFC payload*/
    private void buildNFCTagViews(NdefMessage[] msgs) {
        if (msgs == null || msgs.length == 0) return;

        String text = "";
//        String tagId = new String(msgs[0].getRecords()[0].getType());
        byte[] payload = msgs[0].getRecords()[0].getPayload();
        String textEncoding = ((payload[0] & 128) == 0) ? "UTF-8" : "UTF-16"; // Get the Text Encoding
        int languageCodeLength = payload[0] & 0063; // Get the Language Code, e.g. "en"
        // String languageCode = new String(payload, 1, languageCodeLength, "US-ASCII");

        try {
            // Get the Text
            text = new String(payload, languageCodeLength + 1, payload.length - languageCodeLength - 1, textEncoding);
        } catch (UnsupportedEncodingException e) {
            Log.e("UnsupportedEncoding", e.toString());
        }

        Toast.makeText(getApplicationContext(),"NFC Content: "+text,Toast.LENGTH_SHORT).show();

    }


    @Override
    protected void onNewIntent(Intent intent) {
        setIntent(intent);
        readNFCIntent(intent);
        if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(intent.getAction())){
          String  myTag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
        }
    }


    @Override
    protected void onResume() {
        super.onResume();
        readModeOn();

    }
    private void readModeOn(){
        nfcAdapter.enableForegroundDispatch(this, pendingIntent, writeTagFilters, null);
    }

    @Override
    protected void onPause() {
        super.onPause();
        readModeOff();

    }

    private void readModeOff(){
        nfcAdapter.disableForegroundDispatch(this);
    }

    private void populateTabLayout(List<Doctors> list){
        tabLayout.addTab(tabLayout.newTab().setText("All"));
        for(Doctors doctor: list)
        {
            TabLayout.Tab tempTab = tabLayout.newTab().setText(doctor.getName()); //TODO chenger this to name and get id through another way
            tempTab.setIcon(R.drawable.john_smith_photo);
            tempTab.setTag(doctor.getId());
            tabLayout.addTab(tempTab);
        }


        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(tabLayout.getSelectedTabPosition() == 0){ //All get all appointments for today
                    Toast.makeText(NavDrawerActivity.this, "Tab " + tabLayout.getSelectedTabPosition(), Toast.LENGTH_LONG).show();
                    currentDoctorId = tab.getText().toString();
                    databaseAppointmentRef.orderByChild("startTime").startAt(datePickerToMilliseconds(datePickerDialog)).endAt(dateHelper.getEndOfDayInMilli(datePickerToMilliseconds(datePickerDialog))).addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            displayAllAppointmentsForToday(dataSnapshot);
                            Toast.makeText(NavDrawerActivity.this, "Tabdfscfds " + tabLayout.getSelectedTabPosition(), Toast.LENGTH_LONG).show();
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                }else {
                    currentDoctorId = tab.getText().toString(); //TODO needed?
                    databaseAppointmentRef.addListenerForSingleValueEvent(new ValueEventListener() {
                        @Override
                        public void onDataChange(DataSnapshot dataSnapshot) {

                            currentDoctorId = tabLayout.getTabAt(tabLayout.getSelectedTabPosition()).getTag().toString();
                            updateAppointmentList(dataSnapshot);
                        }

                        @Override
                        public void onCancelled(DatabaseError databaseError) {

                        }
                    });
                    Toast.makeText(NavDrawerActivity.this, "Tab " + tabLayout.getSelectedTabPosition(), Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    private void setEvents(){
        currenDateTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                databaseAppointmentRef.addValueEventListener(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {

                        datePickerDialog.show();
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }

        });

        rightArrowImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                navigateOneDay(1);
            }

        });

        leftArrowImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                navigateOneDay(-1);
            }

        });

        datePickerDialog.setOnDateSetListener(new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker view, final int year, final int month, final int dayOfMonth) {
                databaseAppointmentRef.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        updateAppointmentList(dataSnapshot);
                        datePickerDialog.updateDate(year, month, dayOfMonth);
                        currenDateTextView.setText(df.format(datePickerToMilliseconds(datePickerDialog)));
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });


        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        // TODO Handle item click
                        Appointment a = appointmentList.get(position);
                        showUpdateDialog(a);
                    }
                })
        );
    }

    private void navigateOneDay(int days){
        Calendar calendar = Calendar.getInstance();
        calendar.setTimeInMillis(datePickerToMilliseconds(datePickerDialog));
        calendar.add(Calendar.DATE, days);

        currenDateTextView.setText(df.format(calendar.getTimeInMillis()));
        datePickerDialog.updateDate(calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH));

        databaseAppointmentRef.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                        /*Calendar cal = Calendar.getInstance();
                        cal.set(year, month, dayOfMonth);
                        calendarView.setDate(cal.getTimeInMillis());*/
                updateAppointmentList(dataSnapshot);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });
    }


    private void getIds(){
        currenDateTextView = (TextView) findViewById(R.id.textViewCurrentDate);
        rightArrowImageView = (ImageView) findViewById(R.id.imageViewRigthArrow);
        numAppointmentsTextView = (TextView) findViewById(R.id.textViewNumAppointments);
        doctorSelectedName = (TextView) findViewById(R.id.selectedDoctorName);
        openDatePickerButton = (Button) findViewById(R.id.openDatePickerButton);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_appointments);
        leftArrowImageView = (ImageView) findViewById(R.id.imageViewLeftArrow);

    }
    private boolean updateAppointmentBooked(Appointment appointmentParam) {
        //getting the specified appointments reference
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference(Constants.APPOINTMENT_KEY).child(appointmentParam.getId());

        //updating appointment
        Appointment appointment = new Appointment(appointmentParam.getId(), appointmentParam.getStartTime(), true, appointmentParam.getDoctorId(), appointmentParam.getClinicId());
        dR.setValue(appointment);
        Toast.makeText(getApplicationContext(), "Booking Successful", Toast.LENGTH_LONG).show();
        return true;
    }
    private void showUpdateDialog(final Appointment appointmentParam){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);


        LayoutInflater inflater = getLayoutInflater();

        final View dialogView = inflater.inflate(R.layout.book_appointment_dialog, null);

        dialogBuilder.setView(dialogView);

        final TextView textViewDate = (TextView) dialogView.findViewById(R.id.textview_date);
        final TextView textViewTime = (TextView) dialogView.findViewById(R.id.textview_time);
        final TextView textViewCancel = (TextView) dialogView.findViewById(R.id.textview_cancel);
        final Button bookAppointmentButton = (Button) dialogView.findViewById(R.id.book_appointment_button1);
        final ImageView doctorPhotoImageView = (ImageView) dialogView.findViewById(R.id.imageViewApppointmentDialog);
        final TextView textViewDocName = (TextView) dialogView.findViewById(R.id.textview_book_dialog_docname);


        textViewDate.setText(dateHelper.toAppointmentDate(appointmentParam.getStartTime()));
        textViewTime.setText(dateHelper.toAppointmentTime(appointmentParam.getStartTime()));
        textViewDocName.setText(appointmentParam.getDoctorName());

        Context context = doctorPhotoImageView.getContext();
        int id = context.getResources().getIdentifier(appointmentParam.getDoctorPhoto(), "drawable", context.getPackageName());
        doctorPhotoImageView.setImageResource(id);
        dialogBuilder.setTitle("Confirm Booking");

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();



        bookAppointmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                updateAppointmentBooked(appointmentParam); //TODO uncomment
                createBooking(appointmentParam);
                //syncBookingInAndroidCalender();

                alertDialog.dismiss();
            }

        });
        textViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }

        });
        //alertDialog.dismiss();




    }
    public void syncBookingInAndroidCalender(){
        Calendar beginTime = Calendar.getInstance();
        beginTime.set(2017, 11, 18, 7, 30);
        Calendar endTime = Calendar.getInstance();
        endTime.set(2017, 11, 18, 7, 45);
        Intent intent = new Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTimeInMillis())
                .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTimeInMillis())
                .putExtra(CalendarContract.Events.TITLE, "Yoga")
                .putExtra(CalendarContract.Events.DESCRIPTION, "Group class")
                .putExtra(CalendarContract.Events.EVENT_LOCATION, "The gym")
                .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY)
                .putExtra(Intent.EXTRA_EMAIL, "rowan@example.com,trevor@example.com");
        startActivity(intent);
    }
    public interface DataListener {
        void newDataReceived(Users user);
    }

    public void createBooking(final Appointment appointmentParam){
        AccountsHelper accountsHelper = new AccountsHelper();
        accountsHelper.getCurrentUser(new BookAppointment.DataListener() { //TODO NOTE here is method to get current users details

            @Override
                public void newDataReceived(Users user) {
                Bookings booking = new Bookings(user.getEmail().replace(".", ","),appointmentParam.getStartTime(), appointmentParam.getId(), appointmentParam.getDoctorId(), appointmentParam.getClinicId());
                DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference(Constants.BOOKING_KEY);
                String id = databaseRef.push().getKey();
                booking.setId(id);
                databaseRef.child(id).setValue(booking);
            }
        });


    }

    public List<Doctors> getDoctorsList(String clinicId){ //TO
        FirebaseDatabase.getInstance().getReference(Constants.CLINICS_KEY).child(currentUser.getClinic()).child("doctors")
            .addListenerForSingleValueEvent(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    //iterating through all the nodes
                    for (DataSnapshot appointmentSnapshot : dataSnapshot.getChildren()) {
                        //getting artist
                        Doctors doctor = appointmentSnapshot.getValue(Doctors.class);
                        doctorsList.add(doctor);

                    }

                    populateTabLayout(doctorsList);
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

        return doctorsList;
    }

    public void updateAppointmentList(DataSnapshot dataSnapshot){
        //clearing the previous artist list
        appointmentList.clear();

        //iterating through all the nodes
        for (DataSnapshot appointmentSnapshot : dataSnapshot.getChildren()) {
            //getting artist
            Appointment appointment = appointmentSnapshot.getValue(Appointment.class);
            if((appointment.getDoctorId().equals(currentDoctorId) || currentDoctorId.equals("All") )&& appointment.getClinicId().equals(currentUser.getClinic()) && !appointment.isBooked() && dateHelper.isDateSameDay(datePickerToMilliseconds(datePickerDialog), appointment.getStartTime())){
                //adding appointment to the list
                appointmentList.add(appointment);
            }

        }

        //creating adapter
        mAdapter = new AppointmentAdapter(this, appointmentList);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
        numAppointmentsTextView.setText("Appointments: " + appointmentList.size());
    }



    public void displayAllAppointmentsForToday(DataSnapshot dataSnapshot){
        //clearing the previous artist list
        appointmentList.clear();

        //iterating through all the nodes
        for (DataSnapshot appointmentSnapshot : dataSnapshot.getChildren()) {
            //getting artist
            Appointment appointment = appointmentSnapshot.getValue(Appointment.class);
            if(appointment.getClinicId().equals(currentUser.getClinic()) && !appointment.isBooked()){
                //adding appointment to the list
                appointmentList.add(appointment);
            }

        }

        //creating adapter
        mAdapter = new AppointmentAdapter(this, appointmentList);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();
        numAppointmentsTextView.setText("Appointments: " + appointmentList.size());
    }

    public long datePickerToMilliseconds(DatePickerDialog datePickerDialog){
        Calendar calendar = new GregorianCalendar(datePickerDialog.getDatePicker().getYear(), datePickerDialog.getDatePicker().getMonth(), datePickerDialog.getDatePicker().getDayOfMonth());
        return calendar.getTimeInMillis();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //attaching value event listener
        databaseAppointmentRef.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                updateAppointmentList(dataSnapshot);
            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }


        });
    };

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.nav_drawer, menu);
        //getMenuInflater().inflate(R.menu.menu_main, menu); TODO docs say use this?? https://www.androidtutorialpoint.com/material-design/android-viewpager-example-working-tablayout-android/
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        if (id == R.id.nav_bookings) {
            startActivity(new Intent(NavDrawerActivity.this, ViewBookings.class));
        } else if (id == R.id.nav_gallery) {

        } else if (id == R.id.nav_history) {
            startActivity(new Intent(NavDrawerActivity.this, History.class));
        } else if (id == R.id.nav_clinic_info) {
            startActivity(new Intent(NavDrawerActivity.this, MapsActivity.class));
        } else if (id == R.id.nav_share) {
            Intent myIntent = new Intent(NavDrawerActivity.this, QueueStatusActivity.class);
            myIntent.putExtra("clinic",currentUser.getClinic());
            startActivity(myIntent);
        } else if (id == R.id.nav_send) {

        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    public void onArticleSelected(int position) {
        // The user selected the headline of an article from the HeadlinesFragment
        // Do something here to display that article

        datePickerDialog.show();
    }
}