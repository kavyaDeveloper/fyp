package com.example.steph.fyp.models;

/**
 * Created by steph on 17/11/2017.
 */

public class Users {
    private String user;
    private String email;
    private String photUrl;
    private String Uid;
    private String phoneNum;
    private String clinic;
    private String doctor;



    public Users() {
    }

    public Users(String user) {
        this.user = user;
    }

    public Users(String user, String email, String photUrl, String uid) {
        this.user = user;
        this.email = email;
        this.photUrl = photUrl;
        this.Uid = uid;
        this.clinic = "uniquepushid";
        this.doctor = "doctorId";
    }

    public Users(String user, String email, String photUrl, String uid, String phoneNum, String clinic) {
        this.user = user;
        this.email = email;
        this.photUrl = photUrl;
        this.Uid = uid;
        this.phoneNum = phoneNum;
        this.clinic = clinic;
    }
    public Users(String user, String email, String photUrl, String uid, String phoneNum, String clinic, String doctor) {
        this.user = user;
        this.email = email;
        this.photUrl = photUrl;
        this.Uid = uid;
        this.phoneNum = phoneNum;
        this.clinic = clinic;
        this.doctor = doctor;
    }
    public String getPhoneNum() {
        return phoneNum;
    }

    public void setPhoneNum(String phoneNum) {
        this.phoneNum = phoneNum;
    }

    public String getClinic() {
        return clinic;
    }

    public void setClinic(String clinic) {
        this.clinic = clinic;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPhotUrl() {
        return photUrl;
    }

    public void setPhotUrl(String photUrl) {
        this.photUrl = photUrl;
    }

    public String getUid() {
        return Uid;
    }

    public void setUid(String uid) {
        this.Uid = uid;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }
}