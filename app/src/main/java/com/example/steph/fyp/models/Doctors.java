package com.example.steph.fyp.models;

/**
 * Created by steph on 10/01/2018.
 */

public class Doctors {
    private String name;
    private String photo;
    private String id;

    public Doctors(){

    }

    public Doctors(String name, String photo, String id) {
        this.name = name;
        this.photo = photo;
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

}
