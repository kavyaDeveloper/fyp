package com.example.steph.fyp;

import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.steph.fyp.helpers.DateHelper;
import com.example.steph.fyp.models.Bookings;
import com.example.steph.fyp.utils.Constants;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class ViewBookings extends AppCompatActivity {
    DatabaseReference databaseBookings;
    List<Bookings> bookingsList;
    //ListView listViewBookings;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_bookings);
        databaseBookings = FirebaseDatabase.getInstance().getReference(Constants.BOOKING_KEY);
        bookingsList = new ArrayList<>();

        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view_bookings);

        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(this));



        /*listViewBookings = (ListView) findViewById(R.id.listview_bookings);

        listViewBookings.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //getting the selected appointment
                Bookings booking = bookingsList.get(i);
                showUpdateDialog(booking);
            }
        });*/

        mRecyclerView.addOnItemTouchListener(
                new RecyclerItemClickListener(this, new RecyclerItemClickListener.OnItemClickListener() {
                    @Override public void onItemClick(View view, int position) {
                        // TODO Handle item click
                        /*Appointment a = appointmentList.get(position);
                        showUpdateDialog(a);*/
                    }
                })
        );
    }

    @Override
    protected void onStart() {
        super.onStart();
        //attaching value event listener
        databaseBookings.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                updateBookingstList(dataSnapshot);

            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }


        });
    };

    public void updateBookingstList(DataSnapshot dataSnapshot){
        //clearing the previous artist list
        bookingsList.clear();
        FirebaseUser currentFirebaseUser = FirebaseAuth.getInstance().getCurrentUser() ;

        //iterating through all the nodes
        for (DataSnapshot appointmentSnapshot : dataSnapshot.getChildren()) {
            //getting artist
            Bookings booking = appointmentSnapshot.getValue(Bookings.class);
            if(!booking.isCancelled() && booking.getCustomerId().replace(",", ".").equals(currentFirebaseUser.getEmail())){
                //adding appointment to the list

                bookingsList.add(booking);
            }

        }

        //creating adapter
        mAdapter = new BookingAdapter(this, bookingsList);
        mRecyclerView.setAdapter(mAdapter);
        mAdapter.notifyDataSetChanged();





    }

    public void cancelBooking(Bookings bookingParam){
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference(Constants.BOOKING_KEY).child(bookingParam.getId());

        //updating booking
        Bookings booking = new Bookings(bookingParam.getId(), bookingParam.getStartTime());
        booking.setCancelled(true);
        dR.setValue(null);
        Toast.makeText(getApplicationContext(), "Booking Successfully Cancelled", Toast.LENGTH_LONG).show();

        SetAppointmentAvailable(bookingParam);
    }
    public void SetAppointmentAvailable(Bookings bookingParam){
        String appointmentId = bookingParam.getAppointmentId();
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference(Constants.APPOINTMENT_KEY).child(appointmentId).child("booked");
        dR.setValue(false);

    }

    private void showUpdateDialog(final Bookings booking){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);
        DateHelper dateHelper = new DateHelper();


        LayoutInflater inflater = getLayoutInflater();

        final View dialogView = inflater.inflate(R.layout.cancel_booking_dialog, null);

        dialogBuilder.setView(dialogView);

        final TextView textViewDate = (TextView) dialogView.findViewById(R.id.booking_dialog_date_text);
        final TextView textViewTime = (TextView) dialogView.findViewById(R.id.booking_dialog_time_text);
        final TextView textViewCancel = (TextView) dialogView.findViewById(R.id.booking_dialog_cancel_text);
        final Button cancelBookingButton = (Button) dialogView.findViewById(R.id.cancel_booking_button);

        textViewDate.setText(dateHelper.toAppointmentDate(booking.getStartTime()));
        textViewTime.setText(dateHelper.toAppointmentTime(booking.getStartTime()));
        dialogBuilder.setTitle("Confirm Booking");

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();



        cancelBookingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                //updateAppointmentBooked(appointmentId,appointmentDate); //TODO uncomment
                cancelBooking(booking);
                //syncBookingInAndroidCalender();


                alertDialog.dismiss();
            }

        });
        textViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }

        });
        //alertDialog.dismiss();




    }
}
