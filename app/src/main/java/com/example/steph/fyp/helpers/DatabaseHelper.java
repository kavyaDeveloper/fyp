package com.example.steph.fyp.helpers;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

/**
 * Created by steph on 10/11/2017.
 */

public class DatabaseHelper {
    public void addToDB(Object obj, String tableName){
        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference(tableName);
        String id = databaseRef.push().getKey();
        databaseRef.child(id).setValue(obj);
    }
}
