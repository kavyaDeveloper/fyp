package com.example.steph.fyp.models;

/**
 * Created by steph on 17/11/2017.
 */

public class Bookings {
    private String id;
    private String customerId;
    private String appointmentId;
    private long startTime;
    private boolean isCancelled;
    private long checkInTime;
    private long checkOutTime;
    private boolean isPaid;
    private String doctorId;
    private String clinicId;

    public Bookings() {
    }

    public Bookings(String customerId, long startTime) {
        this.customerId = customerId;
        this.startTime = startTime;
        this.isCancelled = false;
        this.checkOutTime = 0;
    }

    public Bookings(String customerId, long startTime, String appointmentId) {
        this.customerId = customerId;
        this.startTime = startTime;
        this.appointmentId = appointmentId;
        this.isCancelled = false;
        this.checkOutTime = 0;
    }

    public Bookings(String customerId, long startTime, String appointmentId, String doctorId) {
        this.customerId = customerId;
        this.startTime = startTime;
        this.appointmentId = appointmentId;
        this.isCancelled = false;
        this.checkOutTime = 0;
        this.doctorId = doctorId;
        this.isPaid = false;
    }

    public Bookings(String id, String customerId, String appointmentId, long startTime, boolean isCancelled, long checkInTime, long checkOutTime) {
        this.id = id;
        this.customerId = customerId;
        this.appointmentId = appointmentId;
        this.startTime = startTime;
        this.isCancelled = isCancelled;
        this.checkInTime = checkInTime;
        this.checkOutTime = checkOutTime;
    }
    public Bookings(String id, String customerId, String appointmentId, long startTime, boolean isCancelled, long checkInTime, long checkOutTime, boolean isPaid) {
        this.id = id;
        this.customerId = customerId;
        this.appointmentId = appointmentId;
        this.startTime = startTime;
        this.isCancelled = isCancelled;
        this.checkInTime = checkInTime;
        this.checkOutTime = checkOutTime;
        this.isPaid = isPaid;
    }

    public Bookings(String customerId, long startTime, String appointmentId, String doctorId, String clinicId) {
        this.customerId = customerId;
        this.appointmentId = appointmentId;
        this.startTime = startTime;
        this.checkInTime = 0;
        this.isPaid = false;
        this.doctorId = doctorId;
        this.clinicId = clinicId;
        this.isCancelled = false;
        this.checkOutTime = 0;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public String getClinicId() {
        return clinicId;
    }

    public void setClinicId(String clinicId) {
        this.clinicId = clinicId;
    }

    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getAppointmentId() {
        return appointmentId;
    }

    public void setAppointmentId(String appointmentId) {
        this.appointmentId = appointmentId;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public boolean isCancelled() {
        return isCancelled;
    }

    public void setCancelled(boolean cancelled) {
        isCancelled = cancelled;
    }

    public long getCheckInTime() {
        return checkInTime;
    }

    public void setCheckInTime(long checkInTime) {
        this.checkInTime = checkInTime;
    }

    public long getCheckOutTime() {
        return checkOutTime;
    }

    public void setCheckOutTime(long checkOutTime) {
        this.checkOutTime = checkOutTime;
    }

    public boolean isPaid() {
        return isPaid;
    }

    public void setPaid(boolean paid) {
        isPaid = paid;
    }
}
