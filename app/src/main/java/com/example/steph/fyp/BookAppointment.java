package com.example.steph.fyp;

import android.content.Intent;
import android.provider.CalendarContract;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CalendarView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.steph.fyp.helpers.AccountsHelper;
import com.example.steph.fyp.helpers.DatabaseHelper;
import com.example.steph.fyp.helpers.DateHelper;
import com.example.steph.fyp.models.Appointment;
import com.example.steph.fyp.models.Bookings;
import com.example.steph.fyp.models.Users;
import com.example.steph.fyp.utils.Constants;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.iid.FirebaseInstanceId;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class BookAppointment extends AppCompatActivity {

    ListView listViewAppointments;
    DatabaseReference databaseAppointment;

    List<Appointment> appointmentList;
    DateHelper dateHelper = new DateHelper();
    CalendarView calendarView;
    Users currentUser;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_book_appointment);

        //listViewAppointments = (ListView) findViewById(R.id.listview_appointments);
        calendarView = (CalendarView) findViewById(R.id.calendar_view);

        //getting the reference of appointments node
        databaseAppointment = FirebaseDatabase.getInstance().getReference(Constants.APPOINTMENT_KEY);
        appointmentList = new ArrayList<>();

       // updateAppointmentList(dataSnapshot);

        calendarView.setOnDateChangeListener(new CalendarView.OnDateChangeListener() {

            @Override
            public void onSelectedDayChange(CalendarView view, final int year, final int month, final int dayOfMonth) {
                databaseAppointment.addListenerForSingleValueEvent(new ValueEventListener() {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot) {
                        Calendar cal = Calendar.getInstance();
                        cal.set(year, month, dayOfMonth);
                        calendarView.setDate(cal.getTimeInMillis());
                        updateAppointmentList(dataSnapshot);
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError) {

                    }
                });
            }
        });
        listViewAppointments.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //getting the selected appointment
                Appointment appointment = appointmentList.get(i);
                showUpdateDialog(appointment);
            }
        });
        String email = FirebaseAuth.getInstance().getCurrentUser().getEmail(); //TODO clean up code here, move to login success

        DatabaseReference reference = FirebaseDatabase.getInstance().getReference("notifications");
        reference.child(email.replace(".",",")).child("token").setValue(FirebaseInstanceId.getInstance()
                .getToken());

    }

    private boolean updateAppointmentBooked(Appointment appointmentParam) {
        //getting the specified appointments reference
        DatabaseReference dR = FirebaseDatabase.getInstance().getReference(Constants.APPOINTMENT_KEY).child(appointmentParam.getId());

        //updating appointment
        Appointment appointment = new Appointment(appointmentParam.getId(), appointmentParam.getStartTime(), true);
        dR.setValue(appointment);
        Toast.makeText(getApplicationContext(), "Booking Successful", Toast.LENGTH_LONG).show();
        return true;
    }
    private void showUpdateDialog(final Appointment appointmentParam){
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);


        LayoutInflater inflater = getLayoutInflater();

        final View dialogView = inflater.inflate(R.layout.book_appointment_dialog, null);

        dialogBuilder.setView(dialogView);

        final TextView textViewDate = (TextView) dialogView.findViewById(R.id.textview_date);
        final TextView textViewTime = (TextView) dialogView.findViewById(R.id.textview_time);
        final TextView textViewCancel = (TextView) dialogView.findViewById(R.id.textview_cancel);
        final Button bookAppointmentButton = (Button) dialogView.findViewById(R.id.book_appointment_button1);

        textViewDate.setText(dateHelper.toAppointmentDate(appointmentParam.getStartTime()));
        textViewTime.setText(dateHelper.toAppointmentTime(appointmentParam.getStartTime()));
        dialogBuilder.setTitle("Confirm Booking");

        final AlertDialog alertDialog = dialogBuilder.create();
        alertDialog.show();



        bookAppointmentButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                updateAppointmentBooked(appointmentParam); //TODO uncomment
                createBooking(appointmentParam);
                //syncBookingInAndroidCalender();


                alertDialog.dismiss();
            }

        });
        textViewCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                alertDialog.dismiss();
            }

        });
        //alertDialog.dismiss();




    }
    public void syncBookingInAndroidCalender(){
        Calendar beginTime = Calendar.getInstance();
        beginTime.set(2017, 11, 18, 7, 30);
        Calendar endTime = Calendar.getInstance();
        endTime.set(2017, 11, 18, 7, 45);
        Intent intent = new Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTimeInMillis())
                .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTimeInMillis())
                .putExtra(CalendarContract.Events.TITLE, "Yoga")
                .putExtra(CalendarContract.Events.DESCRIPTION, "Group class")
                .putExtra(CalendarContract.Events.EVENT_LOCATION, "The gym")
                .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY)
                .putExtra(Intent.EXTRA_EMAIL, "rowan@example.com,trevor@example.com");
        startActivity(intent);
    }
    public interface DataListener {
        void newDataReceived(Users user);
    }

    public void createBooking(final Appointment appointmentParam){
        AccountsHelper accountsHelper = new AccountsHelper();
        accountsHelper.getCurrentUser(new DataListener() { //TODO NOTE here is method to get current users details

            @Override
            public void newDataReceived(Users user) {
                Bookings booking = new Bookings(user.getEmail().replace(".", ","),appointmentParam.getStartTime(), appointmentParam.getId());
                DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference(Constants.BOOKING_KEY);
                String id = databaseRef.push().getKey();
                booking.setId(id);
                databaseRef.child(id).setValue(booking);
            }
        });


    }

    public void updateAppointmentList(DataSnapshot dataSnapshot){
        //clearing the previous artist list
        appointmentList.clear();

        //iterating through all the nodes
        for (DataSnapshot appointmentSnapshot : dataSnapshot.getChildren()) {
            //getting artist
            Appointment appointment = appointmentSnapshot.getValue(Appointment.class);
            if(!appointment.isBooked() && dateHelper.isDateSameDay(calendarView.getDate(), appointment.getStartTime())){
                //adding appointment to the list
                appointmentList.add(appointment);
            }

        }

        //creating adapter
        AppointmentList appointmentAdapter = new AppointmentList(BookAppointment.this, appointmentList);

        //attaching adapter to the listview
        //expandableListViewAppointments.setAdapter(expandableListViewAdapter);
        listViewAppointments.setAdapter(appointmentAdapter);
        appointmentAdapter.notifyDataSetChanged();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //attaching value event listener
        databaseAppointment.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

               updateAppointmentList(dataSnapshot);

            }


            @Override
            public void onCancelled(DatabaseError databaseError) {

            }


        });
    };

}
