package com.example.steph.fyp;

import android.annotation.SuppressLint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.steph.fyp.helpers.DatabaseHelper;
import com.example.steph.fyp.models.Appointment;
import com.example.steph.fyp.models.Users;
import com.example.steph.fyp.utils.Constants;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.Date;

public class ReceptionistAddUser extends AppCompatActivity {
    EditText fName;
    EditText lName;
    EditText email;
    EditText phone;
    Button addUserButton;
    DatabaseReference databaseRef;

    @SuppressLint("CutPasteId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receptionist_add_user);


        fName = (EditText) findViewById(R.id.fname_textbox);
        lName = (EditText) findViewById(R.id.lname_textbox);
        email = (EditText) findViewById(R.id.email_textbox);
        phone = (EditText) findViewById(R.id.phone_textbox);

        addUserButton = (Button) findViewById(R.id.add_user_button);
        databaseRef = FirebaseDatabase.getInstance().getReference("appointments");

        addUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addAppointment();
            }
        });
    }

    private void addAppointment(){
        String startTime = "12:00";
        long date = new Date().getTime();
        Appointment appointment = new Appointment(date);






            String id = databaseRef.push().getKey();
            appointment.setId(id);
            databaseRef.child(id).setValue(appointment);
            Toast.makeText(this, "Appointment added", Toast.LENGTH_SHORT).show();


            //Toast.makeText(this, "Enter contact info", Toast.LENGTH_SHORT).show();

    }


    private void addUser(){
        String fNameTemp = fName.getText().toString().trim();
        String lNameTemp = lName.getText().toString().trim();
        String emailTemp = email.getText().toString().trim();
        String phoneTemp = phone.getText().toString().trim();
        String premise = "generate me";
        String[] address = {"killina", "carbury"};

        if(!TextUtils.isEmpty(phoneTemp)&& !TextUtils.isEmpty(emailTemp)){
            Users user = new Users(fNameTemp, lNameTemp, emailTemp, phoneTemp);


            DatabaseHelper dbHelper = new DatabaseHelper();

            dbHelper.addToDB(user, Constants.USER_KEY);
            Toast.makeText(this, "users added", Toast.LENGTH_SHORT).show();
        }
        else {
            Toast.makeText(this, "Enter contact info", Toast.LENGTH_SHORT).show();
        }



    }

}
