package com.example.steph.fyp;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.example.steph.fyp.helpers.DateHelper;
import com.example.steph.fyp.models.Bookings;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class BookingList extends ArrayAdapter<Bookings> {

    private Activity context;
    private List<Bookings> bookings;

    public BookingList(Activity context, List<Bookings> bookingList){
        super(context, R.layout.appointmentlist_layout, bookingList);
        this.context = context;
        this.bookings = bookingList;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.bookinglist_layout, null, true);

        TextView textViewDate = (TextView) listViewItem.findViewById(R.id.textview_booking_date);
        TextView textViewStartTime = (TextView) listViewItem.findViewById(R.id.textview_booking_time);

        Bookings booking = bookings.get(position);


        DateFormat df = new SimpleDateFormat("EEE, d MMM");
        Date date = new Date(booking.getStartTime());

        Calendar calendar = Calendar.getInstance();
        calendar.setTime(date);
        int hours = calendar.get(Calendar.HOUR_OF_DAY);
        DateHelper dateHelper = new DateHelper();


        textViewStartTime.setText(dateHelper.toAppointmentTime(booking.getStartTime()));
        textViewDate.setText(df.format(booking.getStartTime()));

        return listViewItem;
    }
}
