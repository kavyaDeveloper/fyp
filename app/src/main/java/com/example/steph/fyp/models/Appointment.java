package com.example.steph.fyp.models;

import java.util.Date;

/**
 * Created by steph on 10/11/2017.
 */

public class Appointment {
    //String startTime;
    private String id;
    private long startTime;
    private boolean booked;
    private String doctorId;
    private String clinicId;
    private String doctorPhoto;
    private String doctorName;

    public Appointment() {
    }

    public Appointment(long startTime) {
        this.booked = false;
        this.startTime = startTime;
    }

    public Appointment(String id, long startTime, boolean booked) {
        this.id = id;
        this.startTime = startTime;
        this.booked = booked;
    }

    public Appointment(String id, long startTime, boolean booked, String doctorId) {
        this.id = id;
        this.startTime = startTime;
        this.booked = booked;
        this.doctorId = doctorId;
    }
    public Appointment(String id, long startTime, boolean booked, String doctorId, String clinicId) {
        this.id = id;
        this.startTime = startTime;
        this.booked = booked;
        this.doctorId = doctorId;
        this.clinicId = clinicId;
    }

    public Appointment(String id, long startTime, boolean booked, String doctorId, String clinicId, String doctorPhoto, String doctorName) {
        this.id = id;
        this.startTime = startTime;
        this.booked = booked;
        this.doctorId = doctorId;
        this.clinicId = clinicId;
        this.doctorPhoto = doctorPhoto;
        this.doctorName = doctorName;
    }

    public String getDoctorPhoto() {
        return doctorPhoto;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public void setDoctorPhoto(String doctorPhoto) {
        this.doctorPhoto = doctorPhoto;
    }

    public String getClinicId() {
        return clinicId;
    }

    public void setClinicId(String clinicId) {
        this.clinicId = clinicId;
    }

    public String getDoctorId() {
        return doctorId;
    }

    public void setDoctorId(String doctorId) {
        this.doctorId = doctorId;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public boolean isBooked() {
        return booked;
    }

    public void setBooked(boolean booked) {
        this.booked = booked;
    }

}
